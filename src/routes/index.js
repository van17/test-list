import Vue from 'vue';
import VueRouter from 'vue-router';
import VueList from '../components/VueList';
import ListDetail from '../views/ListDetail';
import AddList from '../views/AddList';
import { store } from '../store/store';

Vue.use(VueRouter);

export const router = new VueRouter({
    mode: 'history',
    routes: [
        {
          path: '/',
          redirect: '/list',
        },
        {
          path: '/list',
          component: VueList,
        },
        {
          path: '/detail/:id/userinfo/:userId',
          component: ListDetail,
          beforeEnter: (to, from, next) => {
            const paramsId = to.params.id;
            const paramsUserId = to.params.userId;
            store.dispatch('FETCH_DETAIL', paramsId)
                .then(() => {
                  store.dispatch('FETCH_USERINFO', paramsUserId)
                    .then(() => {
                      next();
                    })
                    .catch(error => {
                      console.log(error);
                    })
                })
                .catch(error => {
                  console.log(error);
                })
          }
        },
        {
          path: '/add',
          component: AddList,
        }
    ]
})