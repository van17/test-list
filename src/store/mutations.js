export default {
  SET_LIST(state, list) {
      state.list = list;
  },
  SET_DETAIL(state, id) {
      state.listDetail = id;
  },
  SET_USERINFO(state, userinfo) {
      state.userInfo = userinfo;
  },
}