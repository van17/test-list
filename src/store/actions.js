import axios from 'axios';

export default {
    FETCH_LIST(context) {
      return axios.get('https://jsonplaceholder.typicode.com/albums')
          .then(response => {
              context.commit('SET_LIST', response.data);
              return response;
          })
          .catch(error => {
              console.log(error);
          })
    },
    FETCH_DETAIL(context, payload) {
      return axios.get('https://jsonplaceholder.typicode.com/albums/' + `${payload}`)
          .then(response => {
              context.commit('SET_DETAIL', response.data);
              return response;
          })
          .catch(error => {
              console.log(error);
          })
    },
    FETCH_USERINFO(context, payload) {
      return axios.get('https://jsonplaceholder.typicode.com/users/' + `${payload}`)
          .then(response => {
              context.commit('SET_USERINFO', response.data);
              return response;
          })
          .catch(error => {
              console.log(error);
          })
    }
}